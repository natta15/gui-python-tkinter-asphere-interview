from tkinter import *
import sqlite3
import tkinter.messagebox
import datetime
from  tkinter import ttk
import tkinter as tk
import sys
import os
import datetime
import sys

db = sqlite3.connect('database/BMI.db')
c = db.cursor()


fistName, lastName,  nature = '','',''
age, sex, weight, hight, bmi = 0,0,0,0,0
class data_search():
    def __init__(self, master, *args, **kwargs):
        # tk.LabelFrame.__init__(self, *args, **kwargs)
        self.master = master

        # frame
        self.ds = Frame(self.master, width=1600, height=900, bg='gray')
        self.ds.pack(expand=1, side="top", fill="both", padx=550, pady=130)

        self.search_e = Entry(self.master, width=20 )
        self.search_e.place(x=550, y=110)
        self.search_e.focus()

        self.searchBut = Button(self.master, text="search", width=6, height=1, font=(
            'arial 10 bold'), bg='dark orange', fg='white', command=self.search)
        self.searchBut.place(x=674, y=102)

        
        self.black = Button(self.master, text="เพิ่ม", width=6, height=1, font=(
            'arial 10 bold'), bg='dark orange', fg='white' , command=self.black)
        self.black.place(x=752, y=102)

        self.summary = Button(self.master, text="สรุป", width=6, height=1, font=(
            'arial 10 bold'), bg='dark orange', fg='white' , command=self.summary)
        self.summary.place(x=830, y=102)

        self.master.bind('<Return>', self.search)
        
        # sql = 'SELECT fistName, lastName, age, sex, weight, hight, bmi, nature , id FROM dataBmi '
        # c.execute(sql)
        # data = c.fetchall()    
                

        
    def search(self, *args, **kwargs):
        self.name = self.search_e.get()
        sql = 'SELECT fistName, lastName, age, sex, weight, hight, bmi, nature, id FROM dataBmi where fistName = :name or lastName = :lname'
        c.execute(sql,   {"name": self.name, "lname":self.name})
        data = c.fetchall() 
        print(type(data))
        if(self.name == '' or data == []):
            print()
        else :
            #  c.fetchall()  
            tk.Label(self.ds, text="ชื่อ", anchor="w").grid(row=0, column=0, sticky="ew")
            tk.Label(self.ds, text="นามสกุล", anchor="w").grid(row=0, column=1, sticky="ew")
            tk.Label(self.ds, text="อายุ", anchor="w").grid(row=0, column=2, sticky="ew")
            tk.Label(self.ds, text="เพศ", anchor="w").grid(row=0, column=3, sticky="ew")
            tk.Label(self.ds, text="น้ำหนัก", anchor="w").grid(row=0, column=4, sticky="ew")
            tk.Label(self.ds, text="ส่วนสูง", anchor="w").grid(row=0, column=5, sticky="ew")
            tk.Label(self.ds, text="BMI", anchor="w").grid(row=0, column=6, sticky="ew")
            tk.Label(self.ds, text="ลักษณะ", anchor="w").grid(row=0, column=7, sticky="ew")
            tk.Label(self.ds, text="Edit", anchor="w").grid(row=0, column=8, sticky="ew")
            tk.Label(self.ds, text="Delete", anchor="w").grid(row=0, column=9, sticky="ew")
                
            row = 1

            for r in data:
                fistName, lastName,  nature = r[0], r[1], r[7]
                age, sex, weight, hight, bmi = r[2],r[3],r[4],r[5],r[6]
                self.Fname_l = Label(self.ds, text=str(fistName), anchor="w")
                self.Lname_l = Label(self.ds, text=lastName, anchor="w")
                self.age_l = Label(self.ds, text=age, anchor="w")
                self.sex_l = Label(self.ds, text=sex, anchor="w")
                self.weight_l = Label(self.ds, text=weight, anchor="w")
                self.hight_l = Label(self.ds, text=hight, anchor="w")
                self.bmi_l = Label(self.ds, text=bmi, anchor="w")
                self.nature_l = Label(self.ds, text=nature, anchor="w")
            
                Edit_a = Button(self.ds, text="Edit", command=lambda nr=r[8]: self.Edit(r[8]))
                Delete_a = Button(self.ds, text="Delete", command=lambda nr=r[8]: self.delete(r[8]))
                    #active_cb = tk.Checkbutton(self.ds, onvalue=True, offvalue=False)
                    # if r[1]:
                    #     active_cb.select()
                    # else:
                    #     active_cb.deselect()
                
                self.Fname_l.grid(row=row, column=0, sticky="ew")
                self.Lname_l.grid(row=row, column=1, sticky="ew")
                self.age_l.grid(row=row, column=2, sticky="ew")
                self.sex_l.grid(row=row, column=3, sticky="ew")
                self.weight_l.grid(row=row, column=4, sticky="ew")
                self.hight_l.grid(row=row, column=5, sticky="ew")
                self.bmi_l.grid(row=row, column=6, sticky="ew")
                self.nature_l.grid(row=row, column=7, sticky="ew")
                Edit_a.grid(row=row, column=8, sticky="ew")
                Delete_a.grid(row=row, column=9, sticky="ew")

                row += 1
            
            
           
    def summary(self):
        import summary
        self.master.destroy()
        

    def black(self):
        self.master.destroy()
        import bmi

    def delete(self, nr):
        # DELETE FROM dataBmi WHERE id= ?;
        sql = 'DELETE FROM dataBmi WHERE id= :id'
        c.execute(sql, {'id':nr})
        db.commit()
        self.master.destroy()
        import search
        print ("deleting...nr=", nr)

    def Edit(self, nr , *args, **kwargs ):
        self.date = datetime.datetime.now().strftime("%d/%m/%Y")
        sql = "INSERT INTO edit (dB_id, date)VALUES (?, ?)"
        print(self.date)
        c.execute(sql, (nr, self.date))
        db.commit()
        self.master.destroy()
        import edit



root = Tk()
#data_search(root, text="Hello").pack(side="top", fill="both", expand=True, padx=10, pady=10)
b = data_search(root)
root.geometry("1600x900+-9+0")
root.title("BMI")
root.config(bg='gray')
root.mainloop()
