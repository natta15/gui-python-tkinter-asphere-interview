from tkinter import *
import sqlite3
import tkinter.messagebox
import datetime

db = sqlite3.connect('database/BMI.db')
c = db.cursor()


class data_bmi:
    def __init__(self, master, *args, **kwargs):
        self.master = master

        # frame
        self.center = Frame(self.master, width=600, height=500, bg='gray')
        self.center.pack(expand=1)

       
        # heading layout
        self.heading = Label(
            self.center, text="คำนวณหาค่าดัชนีมวลกาย (BMI)", font=('arial 15 bold'))
        self.heading.place(x=150, y=50)
        
        # input layel
        self.fistName_l = Label(self.center, text="ชื่อ:", font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.fistName_l.place(x=140, y=110) #ขื่อ 

        self.lastName_l = Label(self.center, text='นามสกุล:', font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.lastName_l.place(x=140, y=140) #นามสกุล

        self.age_l = Label(self.center, text='อายุ:', font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.age_l.place(x=140, y=170) #อายุ
        self.age_e = Spinbox(self.center, from_=1, to=100, width=28)
        self.age_e.pack()
        self.age_e.place(x=260, y=173)

        self.sex_l = Label(self.center, text='เพศ:', font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.sex_l.place(x=140, y=200) #sex
        self.variable = StringVar(self.center)
        self.variable.set("Select") # default value
        w = OptionMenu(self.center, self.variable, "ชาย", "หญิง")
        w.pack()
        w.place(x=260, y=197)

        self.weight_l = Label(self.center, text='น้ำหนัก (kg.):', font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.weight_l.place(x=140, y=230) #น้ำหนักตัว

        self.high_l = Label(self.center, text='ส่วนสูง (cm.):', font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.high_l.place(x=140, y=260) #ส่วนสูง

        self.date_l = Label(self.center, text='วันที่ตรวจ:', font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.date_l.place(x=140, y=290) #date

        # input entry
        self.fistName_e = Entry(self.center, width=30)
        self.fistName_e.place(x=260, y=113)
        self.fistName_e.focus()

        self.lastName_e = Entry(self.center, width=30)
        self.lastName_e.place(x=260, y=143)

        #self.age_e = Entry(self.center, width=30)
        #self.age_e.place(x=260, y=173)

        #self.sex_e = Entry(self.center, width=30)
        #self.sex_e.place(x=260, y=203)

        self.weight_e = Entry(self.center, width=30)
        self.weight_e.place(x=260, y=233)

        self.high_e = Entry(self.center, width=30)
        self.high_e.place(x=260, y=263)

        self.now = datetime.datetime.now()
       # self.date_e = Entry(self.center, width=30)
       # self.date_e.place(x=260, y=293)
        self.date_e = Label(self.center, text=self.now.strftime("%d/%m/%Y") , font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.date_e.place(x=260, y=290) #date

        # button
        self.submit = Button(self.center, text="submit", width=6, font=(
            'arial 10 bold'), bg='dark orange', fg='white', command=self.get_data)
        self.submit.place(x=385, y=330)

        self.search = Button(self.center, text="search", width=6, font=(
            'arial 10 bold'), bg='dark orange', fg='white', command=self.search)
        self.search.place(x=144, y=330)

        # self.submit = Button(self.center, text="search", width=6, font=(
        #     'arial 10 bold'), bg='orange', fg='white', command=self.search)
        # self.submit.place(x=285, y=330)
       
        self.master.bind('<Return>', self.get_data)

    def get_data(self,  *args, **kwargs):
        self.fistName = self.fistName_e.get()
        self.lastName = self.lastName_e.get()
        self.age = self.age_e.get()
        self.sex = self.variable.get()
        self.weight = self.weight_e.get()
        self.high = self.high_e.get()
        self.date = datetime.datetime.now().strftime("%d/%m/%Y")
        

        if(self.weight == '' or self.high == '' or self.lastName == '' or self.fistName == '' or self.sex == '' or self.age == '' or self.sex == 'Select'):
            tkinter.messagebox.showwarning(
                'Entry', 'กรุณากรอกข้อมูลให้ครบถ้วน')
        else : 
            sql = "INSERT INTO dataBmi (fistName, lastName, age, sex, weight, hight, date, bmi, status, nature) VALUES(?, ?, ?, ?, ?, ? ,?, ?, ?, ?)"
            self.bmi = float(self.weight)/(float(self.high)/100)**2
            
        
            if(self.bmi < 18.5):
                self.nature = 'ผอม'
                self.status = 0
                tkinter.messagebox.showwarning(
                    'Body Mass Index     ', 'BMI: %.2f \n%s' % (self.bmi, 'น้ำหนักน้อย / ผอม'))
            elif(self.bmi >= 18.5 and self.bmi <= 24.9):
                self.nature = 'ปกติ'
                self.status = 1
                tkinter.messagebox.showinfo(
                    'Body Mass Index     ', 'BMI: %.2f \n%s' % (self.bmi, "ปกติ (สุขภาพดี)"))
            elif(self.bmi >= 24.9 and self.bmi <= 29.9):
                self.nature = 'ท้วม'
                self.status = 2
                tkinter.messagebox.showwarning(
                    'Body Mass Index     ', 'BMI: %.2f \n%s' % (self.bmi, "ท้วม / โรคอ้วนระดับ 1"))
            elif(self.bmi >= 30 and self.bmi <= 39.9):
                self.nature = 'อ้วน'
                self.status = 3
                tkinter.messagebox.showwarning(
                    'Body Mass Index     ', 'BMI: %.2f \n%s' % (self.bmi, "อ้วน / โรคอ้วนระดับ 2"))
            elif(self.bmi >= 40):
                self.nature = 'อ้วนมาก'
                self.status = 4
                tkinter.messagebox.showerror('Body Mass Index     ', 'BMI: %.2f \n%s' % (
                    self.bmi, "อ้วนมาก / โรคอ้วนระดับ 3"))
            
            
            c.execute(sql, (self.fistName, self.lastName, self.age, self.sex, self.weight, self.high, self.date, round(self.bmi, 2), self.status, self.nature))
            db.commit()

            # self.master.destroy()
            # import search
        #tkinter.messagebox.showinfo('Body Mass Index     ','BMI: %.2f \n%s' %(self.bmi,self.message))


    # def search(self):
    #     self.master.destroy()
    #     import search

    def search(self):
        self.master.destroy()
        import search
        
        


root = Tk()
b = data_bmi(root)
root.geometry("1600x900+-9+0")
root.title("BMI")
root.config(bg='black')
root.mainloop()



