from tkinter import *
import sqlite3
import tkinter.messagebox
import datetime

from tkinter import ttk
import tkinter as tk
import sqlite3

db = sqlite3.connect('database/BMI.db')
c = db.cursor()


class data_bmi:
    def __init__(self, master, *args, **kwargs):
        self.master = master

        # frame
        self.center = Frame(self.master, width=600, height=500, bg='gray')
        self.center.pack(expand=1, side="top", fill="both", padx=150, pady=130)

        self.center1 = Frame(self.master, width=600, height=500, bg='gray')
        self.center1.pack(expand=1, side="top", fill="both", padx=150, pady=50)

        # heading layout
        self.heading = Label(
            self.master, text="สรุปค่าดัชนีมวลกาย (BMI)", font=('arial 15 bold'))
        self.heading.place(x=650, y=100)
        
        sql = 'SELECT fistName, lastName, age, sex, bmi, nature , id FROM dataBmi '
        c.execute(sql)
        data = c.fetchall() 

        tree = ttk.Treeview(self.center, column=("c1", "c2", "c3", "c4", "c5"), show='headings')
        tree.column("#1", anchor=tk.CENTER)
        tree.heading("#1", text="ชื่อ")
        tree.column("#2", anchor=tk.CENTER)
        tree.heading("#2", text="นามสกุล")
        tree.column("#3", anchor=tk.CENTER)
        tree.heading("#3", text="อายุ")
        tree.column("#4", anchor=tk.CENTER)
        tree.heading("#4", text="เพศ")
        tree.column("#5", anchor=tk.CENTER)
        tree.heading("#5", text="BMI")
        

        tree.pack()   

        for row in data:
            tree.insert("", tk.END, values=row)        


        sql = 'SELECT count(id) FROM dataBmi'
        c.execute(sql)
        data = c.fetchall()
        for r in data :
            self.count = r[0]

        self.fistName_l = Label(self.master, text="จำนวนผู้ที่ทดสอบ:", font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.fistName_l.place(x=580, y=390) #ขื่อ 
        self.fistName_l = Label(self.master, text=self.count, font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.fistName_l.place(x=850, y=390) #ขื่อ 

        sql = 'SELECT count(sex) FROM dataBmi where sex = "ชาย"'
        c.execute(sql)
        data = c.fetchall()
        for r in data :
            self.count = r[0]
        self.fistName_l = Label(self.master, text="จำนวนผู้ที่ทดสอบ (ชาย):", font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.fistName_l.place(x=580, y=420) #ขื่อ 
        self.fistName_l = Label(self.master, text=self.count, font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.fistName_l.place(x=850, y=420) #ขื่อ 

        sql = 'SELECT count(sex) FROM dataBmi where sex = "หญิง"'
        c.execute(sql)
        data = c.fetchall()
        for r in data :
            self.count = r[0]
        self.fistName_l = Label(self.master, text="จำนวนผู้ที่ทดสอบ (หญิง):", font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.fistName_l.place(x=580, y=450) #ขื่อ 
        self.fistName_l = Label(self.master, text=self.count, font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.fistName_l.place(x=850, y=450) #ขื่อ 

        sql = 'SELECT count(status) FROM dataBmi where status = 4'
        c.execute(sql)
        data = c.fetchall()
        for r in data :
            self.count = r[0]
        self.fistName_l = Label(self.master, text="อ้วนมาก:", font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.fistName_l.place(x=580, y=480) #ขื่อ 
        self.fistName_l = Label(self.master, text=self.count, font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.fistName_l.place(x=650, y=480) #ขื่อ 
        
        sql = 'SELECT count(status) FROM dataBmi where status = 3'
        c.execute(sql)
        data = c.fetchall()
        for r in data :
            self.count = r[0]
        self.fistName_l = Label(self.master, text="อ้วน:", font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.fistName_l.place(x=680, y=480) #ขื่อ 
        self.fistName_l = Label(self.master, text=self.count, font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.fistName_l.place(x=720, y=480) #ขื่อ 

        sql = 'SELECT count(status) FROM dataBmi where status = 2'
        c.execute(sql)
        data = c.fetchall()
        for r in data :
            self.count = r[0]
        self.fistName_l = Label(self.master, text="ท้วม:", font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.fistName_l.place(x=750, y=480) #ขื่อ 
        self.fistName_l = Label(self.master, text=self.count, font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.fistName_l.place(x=800, y=480) #ขื่อ 

        sql = 'SELECT count(status) FROM dataBmi where status = 1'
        c.execute(sql)
        data = c.fetchall()
        for r in data :
            self.count = r[0]
        self.fistName_l = Label(self.master, text="ปกติ:", font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.fistName_l.place(x=830, y=480) #ขื่อ 
        self.fistName_l = Label(self.master, text=self.count, font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.fistName_l.place(x=880, y=480) #ขื่อ 

        sql = 'SELECT count(status) FROM dataBmi where status = 0'
        c.execute(sql)
        data = c.fetchall()
        for r in data :
            self.count = r[0]
        self.fistName_l = Label(self.master, text="ผอม:", font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.fistName_l.place(x=900, y=480) #ขื่อ 
        self.fistName_l = Label(self.master, text=self.count, font=(
            'arial 12 bold'), bg='gray', fg='white')
        self.fistName_l.place(x=950, y=480) #ขื่อ 


        sql = 'SELECT fistName, lastName, age FROM dataBmi where (status = 4 or status = 3) and sex = "หญิง" and (age >= 20 and age <= 30) ORDER by bmi DESC LIMIT 0, 3'
        c.execute(sql)
        data = c.fetchall()

        tree = ttk.Treeview(self.center1, column=("c1", "c2", "c3"), show='headings')
        tree.column("#1", anchor=tk.CENTER)
        tree.heading("#1", text="ชื่อ")
        tree.column("#2", anchor=tk.CENTER)
        tree.heading("#2", text="นามสกุล")
        tree.column("#3", anchor=tk.CENTER)
        tree.heading("#3", text="อายุ")

        tree.pack()   

        for r in data :
            tree.insert("", tk.END, values=r)
        # for row in data:
        #     print(row) 
        #     tree.insert("", tk.END, values=row)
                    

        self.submit = Button(self.master, text="search", width=6, font=(
            'arial 10 bold'), bg='dark orange', fg='white', command=self.search)
        self.submit.place(x=1243, y=103)


    def search (self):
        import search
        self.master.destroy()
        




root = Tk()
b = data_bmi(root)
root.geometry("1600x900+-9+0")
root.title("BMI")
root.config(bg='gray')
root.mainloop()



